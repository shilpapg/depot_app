class Product < ActiveRecord::Base
	has_attached_file :image, styles: {large: "600*600>", thumb:"150*150#"}
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
end

